# instance the provider
provider "libvirt" {
  uri = "qemu:///system"
}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "base-image" {
  name = "base-image"
  #source = "https://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.img"
  source = var.infra-os-base
  format = "qcow2"
  pool = var.kvm_pool
}

# 25GB image size
resource "libvirt_volume" "base-image-resized" {
  name           = "disk"
  base_volume_id = libvirt_volume.base-image.id
#  pool           = "default"
  size           = 26843545600
  pool = var.kvm_pool
}

# 25GB emty disk size
resource "libvirt_volume" "base-data-disk" {
  name           = "base-data-disk"
  size           = 26843545600
  pool = var.kvm_pool
}


data "template_file" "user_data" {
  template = "${file("${path.module}/configs/user_config.cfg")}"
}
