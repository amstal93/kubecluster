#!/bin/bash
case $1 in
	on)
		mv loadbalancer.tf loadbalancer.tf_ 2>/dev/null
		mv master2.tf master2.tf_ 2>/dev/null
		mv master3.tf master3.tf_ 2>/dev/null
		mv worker2.tf worker2.tf_ 2>/dev/null
		mv worker3.tf worker3.tf_ 2>/dev/null


	;;

	off)
		mv loadbalancer.tf_ loadbalancer.tf 2>/dev/null
		mv master2.tf_ master2.tf 2>/dev/null
		mv master3.tf_ master3.tf 2>/dev/null
		mv worker2.tf_ worker2.tf 2>/dev/null
		mv worker3.tf_ worker3.tf 2>/dev/null
	;;

	*)
		echo "Only allowed 'on' or 'off' options"
	;;
esac
